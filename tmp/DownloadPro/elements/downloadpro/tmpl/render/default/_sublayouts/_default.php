<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

if ($file) {
	
	if ($limit_reached) {
		echo JText::_('PLG_ZLELEMENTS_DWP_DOWNLOAD_LIMIT_REACHED');
	} else {
		$target = $params->find('layout._target') ? ' target="_blank"' : '';
		echo '<a href="'.JRoute::_($download_link).'" title="'.$download_name.'"'.$target.'>'.$download_name.'</a>';
	}
	
} else {
	echo JText::_('PLG_ZLELEMENTS_DWP_NO_FILE_SELECTED');
}
