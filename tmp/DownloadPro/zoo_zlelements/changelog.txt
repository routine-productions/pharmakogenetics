Changelog
------------

3.1.2
 - Removed deprecated helper
 ^ Install script improvements

3.1.1
 # Language install issues fixed

3.1
 ^ Elements language files are installed in the admin folder now

3.0.1
 ^ Improved install script

3.0
 ^ ZOO & Joomla 3 compatibility
 + Individual Element install scripts
 - j1.5 compatibility removed


* -> Security Fix
# -> Bug Fix
$ -> Language fix or change
+ -> Addition
^ -> Change
- -> Removed
! -> Note