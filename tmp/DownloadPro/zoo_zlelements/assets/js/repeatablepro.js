/* Copyright (C) YOOtheme GmbH, YOOtheme Proprietary Use License (http://www.yootheme.com/license) */
/* Original version by YOOtheme, Pro ehnanced version by ZOOlanders.com */
(function ($) {
    var a = function () {};
    $.extend(a.prototype, {
        name: "ElementRepeatablePro",
        options: {
			url: null,
			msgEditElement: null,
            msgDeleteElement: null,
            msgSortElement: null,
			tinymceUrl: null
        },
        initialize: function (e, a) {
            this.options = $.extend({}, this.options, a);
            var d = this,
                c = e.find("ul.repeatable-list.main"),
				repeatables = c.find("li.repeatable-element.main"),
                index = repeatables.length,
				loader = $("<span>").appendTo(e.find(".add.main"));
			
			// for each repeatable element
			repeatables.each(function () {
				d.attachButtons($(this));
				d.applySubRepeatable($(this))
            });
			
			// Set buttons and sort functions
			c.delegate("span.btn-sort", "mousedown", function () {
				// sort Tab action
				c.find(".more-options.show-advanced").removeClass("show-advanced")
				$(this).closest("li.repeatable-element").find(".content-row").hide();
			}).delegate("span.btn-delete", "click", function () {
				// delete Tabs action
				$(this).closest("li.repeatable-element").first().fadeOut(200, function () {
					$(this).remove()
				})
			}).delegate(".preview", "click", function () {
				// Open/Close Tabs
				$(this).closest("li.repeatable-element").find(".content-row").first().toggle()
			}).sortable({
				// enable sorting
				handle: "span.btn-sort.main",
				placeholder: "repeatable-element main dragging",
                axis: "y",
                opacity: 1,
                delay: 100,
                cursorAt: {
                    top: 16
                },
				revert: 1,
                tolerance: "pointer",
                containment: "parent",
                scroll: !1,
				start: function (b, a) {
					a.placeholder.height(a.item.height() - 2);
					a.placeholder.width(a.item.find("div.repeatable-content").width() - 2)
				}
			});
			
			// on each new element
			e.find("div.add.main a").bind("click", function () {
				loader.addClass("zl-loader");
				$.post(d.options.url, {method: 'getrepeatable', 'args[0]': '_edit', 'args[1]': index++, 'args[2]': true}, function(data) {
					
					var a = $("<li>").addClass("repeatable-element main").html(data);
					
					d.attachButtons(a);
					a.appendTo(c);
					a.find("input, textarea").each(function () {
						$(this).val() && $(this).val("")
					});
					loader.removeClass("zl-loader");
					
					// set SubRepeatables actions
					d.applySubRepeatable(a);
					
					// apply the TinyMce loader
					a.ElementTinyMce({ tinymceUrl: d.options.tinymceUrl });
				});
			})
        },
        attachButtons: function (a) {
			var g = a.children().wrapAll($("<div>").addClass("repeatable-content")),
				f = $("<span>").addClass("tools").appendTo(a.find(".resume-row.main"));
				
			$("<span>").addClass("btn-delete main").attr("title", this.options.msgDeleteElement).appendTo(f);
            $("<span>").addClass("btn-sort main").attr("title", this.options.msgSortElement).appendTo(f)
        },
		applySubRepeatable: function (a) {
			a.find('.sub-repeat-elements').ElementSubRepeatable({ url: this.options.url, msgDeleteElement: this.options.msgDeleteElement, msgSortElement: this.options.msgSortElement, tinymceUrl: this.options.tinymceUrl });
		}
    });
    $.fn[a.prototype.name] = function () {
        var e = arguments,
            f = e[0] ? e[0] : null;
        return this.each(function () {
            var d = $(this);
            if (a.prototype[f] && d.data(a.prototype.name) && f != "initialize") d.data(a.prototype.name)[f].apply(d.data(a.prototype.name), Array.prototype.slice.call(e, 1));
            else if (!f || $.isPlainObject(f)) {
                var c = new a;
                a.prototype.initialize && c.initialize.apply(c, $.merge([d], e));
                d.data(a.prototype.name, c)
            } else $.error("Method " + f + " does not exist on jQuery." + a.name)
        })
    }
})(jQuery);
(function ($) {
    var a = function () {};
    $.extend(a.prototype, {
        name: "ElementSubRepeatable",
        options: {
			url: null,
			msgEditElement: null,
            msgDeleteElement: null,
            msgSortElement: null,
			tinymceUrl: null
        },
        initialize: function (e, a) {
            this.options = $.extend({}, this.options, a);
            var d = this,
                c = e.find("ul.sub-repeatable-list"),
				index = e.data("index"),
				name = e.data("name"),
				repeatables = c.find("li.repeatable-element.sub"),
                subindex 	= repeatables.length,
				loader 		= $("<span>").appendTo(e.find("div.add.sub"));
				
            repeatables.each(function () {
                d.attachButtons($(this))
            });
			
			c.sortable({
                handle: "span.btn-sort.sub",
                placeholder: "repeatable-element sub dragging",
                axis: "y",
                opacity: 1,
                delay: 100,
                cursorAt: {
                    top: 16
                },
				revert: 1,
                tolerance: "pointer",
                containment: "parent",
                scroll: !1,
                start: function (b, a) {
                    a.placeholder.height(a.item.height() - 2);
                    a.placeholder.width(a.item.find("div.repeatable-content").width() - 2)
                }
            });
            e.find("div.add.sub a").bind("click", function() {
				var layout = $(this).data("layout");
				loader.addClass("zl-loader");
				$.post(d.options.url, {method: 'getsubrepeatable', 'args[0]': layout, 'args[1]': name, 'args[2]': index, 'args[3]': subindex++}, function(data) {

					var a = $("<li>").addClass("repeatable-element sub").html(data);
					
					d.attachButtons(a);
					a.appendTo(c);
					loader.removeClass("zl-loader");
					
					// apply the TinyMce loader
					a.ElementTinyMce({ tinymceUrl: d.options.tinymceUrl });
				});
            })
        },
        attachButtons: function (a) {
			var g = a.children().wrapAll($("<div>").addClass("sub-repeatable-content")),
				f = $("<span>").addClass("tools").appendTo(a.find(".resume-row"));
			
            $("<span>").addClass("btn-delete sub").attr("title", this.options.msgDeleteElement).appendTo(f);
			$("<span>").addClass("btn-sort sub").attr("title", this.options.msgSortElement).appendTo(f);
        }
    });
    $.fn[a.prototype.name] = function () {
        var e = arguments,
            f = e[0] ? e[0] : null;
        return this.each(function () {
            var d = $(this);
            if (a.prototype[f] && d.data(a.prototype.name) && f != "initialize") d.data(a.prototype.name)[f].apply(d.data(a.prototype.name), Array.prototype.slice.call(e, 1));
            else if (!f || $.isPlainObject(f)) {
                var c = new a;
                a.prototype.initialize && c.initialize.apply(c, $.merge([d], e));
                d.data(a.prototype.name, c)
            } else $.error("Method " + f + " does not exist on jQuery." + a.name)
        })
    }
})(jQuery);