<?php
$URI = array_values(array_filter(explode('/', $_SERVER['REQUEST_URI'])));
if ($URI[0] != 'authors' && $URI[0] != 'editorial' && $URI[0] != 'journals') {
// Get Keywords from Title
    $SearchWords = $this->item->name;

    $SearchWords = str_replace(',', '', $SearchWords);
    $SearchWords = str_replace('.', '', $SearchWords);
    $SearchWords = explode(' ', $SearchWords);

    $CurrentTime = date('Y-m-d H:i:s');

    foreach ($SearchWords as $KeySearchWord => &$SearchWord) {
        if (mb_strlen($SearchWord) < 4) {
            unset($SearchWords[$KeySearchWord]);
        }
        $SearchWord = mb_substr($SearchWord, 0, -1);
    }
//echo '<meta charset="utf-8">';
//print_r($SearchWords);exit;

// Get Corresponding Items By Keys
    $DB = JFactory::getDBO();

    $Query = "SELECT * FROM #__zoo_item WHERE state = 1 AND type != 'journals' AND ('$CurrentTime' > publish_up OR publish_up = '0000-00-00 00:00:00' ) AND ('$CurrentTime' < publish_down OR publish_down = '0000-00-00 00:00:00' ) AND (";

    foreach ($SearchWords as $KeySearchWord => $SearchWord) {
        // Thing
        if (prev($SearchWords)) {
            $Query .= "name LIKE '%" . $SearchWord . "%'";
        } else {
            $Query .= " OR name LIKE '%" . $SearchWord . "%'";
        }
    }
    $Query .= ") AND name != '" . $this->item->name . "'";


    $DB->setQuery($Query);

    $RecentItems = $DB->loadAssocList();
    if (!$RecentItems) {
        $Query = "SELECT * FROM #__zoo_item WHERE state = 1 AND ('$CurrentTime' > publish_up OR publish_up = '0000-00-00 00:00:00' ) AND ('$CurrentTime' < publish_down OR publish_down = '0000-00-00 00:00:00' ) AND";
        $Query .= " name != '" . $this->item->name . "' ORDER BY hits DESC LIMIT 5 ";
        $DB->setQuery($Query);

        $RecentItems = $DB->loadAssocList();
    }
    ?>

    <div class="details alignment-left">
        <div class="heading">
            <h1 class="title">Похожие статьи</h1>
        </div>
    </div>


    <div class="items items-col-1">
        <div class="Grid Merge width100 first last">
            <?php


            foreach ($RecentItems as $KeyItem => &$Item) {
                $Query = "SELECT * FROM #__zoo_category_item as rel LEFT JOIN #__zoo_category category ON category.id=rel.category_id  WHERE rel.item_id=" . $Item['id'];
                $DB->setQuery($Query);

                $Categories = $DB->loadAssocList();
                $Item['elements'] = (array)json_decode($Item['elements']);


                foreach ($Item['elements'] as $SubItem) {

                    $SubItem = $SubItem;

                    $New = $SubItem;

                    if (array_key_exists('file', (array)$SubItem->{0}) && array_key_exists('width', (array)$SubItem->{0})) {
                        $RecentItems[$KeyItem]['file'] = $SubItem->{0}->{'file'};
                    }


                }

                if ($KeyItem > 9) {
                    break;
                }
                ?>


                <div class="teaser-item">
                    <div class="teaser-item-bg">

                        <div class="pos-item">

                            <h1 class="pos-title">
                                <img src="/<?= $Item['file'] ?>">
                                <?php foreach ($Categories as $Category) { ?>
                                    <article class="item">
                                        <a href="/"><?= $Category['name'] ?></a>
                                    </article>
                                <?php } ?>
                            </h1>

                            <p class="pos-meta">1</p>

                            <h2 class="pos-subtitle">
                                <a href="/item/<?= $Item['alias'] ?>" class="Active">
                                    <?= $Item['name'] ?>
                                </a>
                            </h2>

                            <div class="floatbox">
                                <div class="pos-content">
                                    <div class="element element-itemcreated first">
                                        <?php setlocale(LC_ALL, 'ru_RU.UTF-8'); ?>
                                        <?= strftime('%a, %d %b %Y', strtotime($Item['created'])); ?>


                                    </div>
                                    <div class="element element-itemhits last">
                                        <?= $Item['hits'] ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
<?
}