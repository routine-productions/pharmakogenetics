<?php
//error_reporting(E_ALL);

$URI = array_values(array_filter(explode('/', $_SERVER[REQUEST_URI])));

if ($URI[0] == 'journals' && isset($URI[1])) {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Get Journal
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $DB = JFactory::getDBO();
    $CurrentTime = date('Y-m-d H:i:s');
    $Query = "SELECT * FROM `#__zoo_item` `item`
          WHERE `type` = 'journal' AND state = 1
          ORDER BY `item`.`name` DESC
          LIMIT 0,10
          ";

    $DB->setQuery($Query);
    $Journal = $DB->loadAssoc();


    $Journal['elements'] = (array)json_decode($Journal['elements'], true);
    $I = 0;
    foreach ($Journal['elements'] as $Key => $SubItem) {
        $I++;
        unset($Journal['elements'][$Key]);
        $Journal['elements'][$I] = (array)$SubItem;
    }

//print_r($Journal);
//exit;

    if (!$Journal['elements'][2][0]['file']) {
        $Journal['elements'][2][0]['file'] = '/images/no-image.png';
    }

//print_r($Journal['elements']);exit;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Get Articles
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $ArticlesIds = implode(',', $Journal['elements'][4]['item']);

    $Query = "SELECT *, `item`.`name` AS `article_name`, `item`.`id` AS `article_id`, `item`.`alias` AS `article_alias` FROM `#__zoo_item` `item`
              LEFT JOIN  `#__zoo_category_item` `rel`  ON (`item`.`id` = `rel`.`item_id`)
              LEFT JOIN  `#__zoo_category` `cat`  ON (`cat`.`id` = `rel`.`category_id`)
              WHERE `item`.`id` IN ($ArticlesIds) AND `item`.`state`= 1 AND ('$CurrentTime' > `item`.`publish_up` OR `item`.`publish_up` = '0000-00-00 00:00:00')
              AND `cat`.`id` NOT IN (89,92)
              ORDER BY `cat`.`ordering` DESC
          ";

    $DB->setQuery($Query);
    $JournalItems = $DB->loadAssocList();


    foreach ($JournalItems as $Key => &$JournalItem) {
        $JournalItem['elements'] = (array)json_decode($JournalItem['elements']);
        $I = 0;
        foreach ($JournalItem['elements'] as $Key => $SubItem) {
            $I++;
            unset($JournalItem['elements'][$Key]);
            $JournalItem['elements'][$I] = (array)$SubItem;
        }
    }


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Get Authors
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    foreach ($JournalItems as $Key => &$JournalItem) {

        // Too Bad...
        $AuthorsAndJournals = array_merge((array)$JournalItem['elements'][5]['item'], (array)$JournalItem['elements'][4]['item']);
        $AuthorsIds = implode(',', $AuthorsAndJournals);
        if ($AuthorsIds) {
            $Query = "SELECT * FROM `#__zoo_item` `item`
              WHERE `item`.`id` IN ($AuthorsIds) AND `item`.`type`='author' AND `item`.`state`= 1 AND ('$CurrentTime' > `item`.`publish_up` OR `item`.`publish_up` = '0000-00-00 00:00:00')";

            $DB->setQuery($Query);
//        print_r();
            $JournalItem['authors'] = $DB->loadAssocList();
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Sort By Category
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $Article_Groups = array();

    foreach ($JournalItems as $Key => &$JournalItem) {
        $Article_Groups[$JournalItem['id']][] = $JournalItem;
    }


    usort($Article_Groups, function ($a, $b) {
        return $a[0]['ordering'] - $b[0]['ordering'];
    });


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Get All Journals
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $DB = JFactory::getDBO();
    $CurrentTime = date('Y-m-d H:i:s');
    $Query = "SELECT * FROM `#__zoo_item` `item`
          WHERE state = 1 AND ('$CurrentTime' > `item`.`publish_up` OR `item`.`publish_up` = '0000-00-00 00:00:00' )
                AND `type` = 'journal'
          ORDER BY `item`.`name` DESC
          ";

    $DB->setQuery($Query);
    $AllJournals = $DB->loadAssocList();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    echo '<h2 class="Journal-Block-Title">' . $Journal['name'] . '</h2>';
    echo '<div class="Journal-Wrapper">';

    echo '<div class="Content-Journal-Wrapper">';
    echo '<div class="About-Journal Shadows Current-Main-Journal">';
    echo '<div class="Journal-Left-Image"><img src="' . $Journal['elements'][1][0]['file'] . '" />';
    echo '</div>';


    echo '<div class="About-Journal-Info">';
    echo $Journal['elements'][3][0]['value'] . '<div>' . $Journal['elements'][4][0]['value'] . '</div>
<span class="About-Journal-Hits"><i class="Eye"></i><span>' . $Journal['hits'] . '</span></span>
';
    if (!empty($Journal['elements'][2][0]['file'])) {
        echo '<a class="PDF" href="' . $Journal['elements'][2][0]['file'] . '" target="_blank">Скачать PDF</a>';
    }


    echo '</div></div>';


    echo '<div class="Shadows">';
    foreach ($Article_Groups as $Key_Group => $Group) {
//    echo '<a href="/journals?filter_category=' . $Group[0]['alias'] . '&filter_author=all"><h3 class="Group-Title">' . $Group[0]['name'] . '</h3></a>';
//        echo '<div  class="Group-Title-Block"><h3 class="Group-Title">' . $Group[0]['name'] . '</h3></div>';
        echo '<a class="Group-Title-Block" href="/articles/category/' . $Group[0]['alias'] . '"><h3 class="Group-Title">' . $Group[0]['name'] . '</h3></a>';

        echo '<div class="Article-Group">';
        foreach ($Group as $Key_Article => $Article) {
            echo '<div class="Article-Box">';
            echo '<a href="/articles/item/' . $Article['article_alias'] . '"><h4 class="Article-Title">' . $Article['article_name'] . '</h4></a>';
            echo '<span class="Article-Hits">' . $Article['hits'] . '</span>';

            if ($Article['authors']) {
                echo '<div class="Article-Authors">';
                $Authors_View = array();
                echo '<span>Авторы:</span>';
                foreach ($Article['authors'] as $Key_Author => $Author) {
                    $Authors_View[] = '<a href="/authors/item/' . $Author['alias'] . '">' . $Author['name'] . '</a>';
                }

                echo implode('', $Authors_View);
                echo '</div>';
            }

            echo '</div>';
        }
        echo '</div>';
    }
    echo '</div>';
    echo '</div>';


    echo '</div>';
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////