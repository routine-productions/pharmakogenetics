<?php
/**
* @package		ZOOorder
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config & Helper
require_once (JPATH_ADMINISTRATOR.'/components/com_zoo/config.php');

// get app
$zoo = App::getInstance( 'zoo' );

// load zoo frontend language file
$zoo->system->language->load('com_zoo');

if(JPluginHelper::isEnabled('system', 'zooorder'))
{
	// Get the app & type
	if ($application = $zoo->table->application->get($params->get('application', 0))) 
	{
		// Add ordering
		$zoo->path->register($zoo->path->path('zooorder:renderer'), 'renderer');
		$order_renderer = $zoo->renderer->create('ordering')->addPath(array( $zoo->path->path('zooorder:')) );
		$elements_layout = $params->get('layout', 'default');
		
		$type = $application->getType($params->get('type'));
			
		// Get url params for non SEF sites	
		$url_params = JURI::getInstance()->getQuery(true);
		
		$elements = $order_renderer->render('item.'.$elements_layout, compact('type') );
		$module_layout = $params->get('module_layout', 'default');

		include ( JModuleHelper::getLayoutPath('mod_zooorder', $module_layout) );
	} 
	else 
	{
		echo JText::_('PLG_ZOOORDER_MOD_NO_APP');	
	}
}
else 
{
	echo 'ZOOorder Plugin is not installed and/or published'; // we can't translate this text if plugin is not enabled
}