<?php
/**
* @package		ZOOorder
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$default_ordering = JRequest::getVar('ordertype', '') ? JRequest::getVar('ordertype', '') : @$elements[0]['ordering'];

?>

<form action="<?php echo JURI::getInstance()->current(); ?>" method="GET">
	<?php if( count( $elements) ): ?>
		
		<?php foreach($url_params as $key => $param): ?>
			<input type="hidden" name="<?php echo $key;?>" value="<?php echo $param;?>" />
		<?php endforeach; ?>
	
		<select name="order" class="zooorder-order">
			<?php foreach( $elements as $element ): ?>
			<option data-ordering="<?php echo $element['ordering']; ?>" value="<?php echo $element['element']->identifier; ?>" <?php echo (JRequest::getVar('order', '') == $element['element']->identifier) ? 'selected="selected"' : ''; ?>>
				<?php echo $element['label']; ?>
			</option>
			<?php endforeach; ?>	
		</select>
		<select name="direction">
			<option value="asc" <?php echo (JRequest::getVar('direction', '') == 'asc') ? 'selected="selected"' : ''; ?>><?php echo JText::_('PLG_ZOOORDER_ASCENDING'); ?></option>
			<option value="desc" <?php echo (JRequest::getVar('direction', '') == 'desc') ? 'selected="selected"' : ''; ?>><?php echo JText::_('PLG_ZOOORDER_DESCENDING'); ?></option>		
		</select>

		<input type="hidden" name="ordertype" class="zoorder-ordertype" value="<?php echo $default_ordering; ?>" />
		<input type="hidden" name="page" value="1" />
		<input type="hidden" name="application" value="<?php echo $application->id; ?>" />
		<input type="submit" value="<?php echo JText::_('PLG_ZOOORDER_SORT'); ?>"/>
	
	<?php else: ?>
		<?php echo JText::_('PLG_ZOOORDER_MOD_NO_ELEMENTS'); ?>
	<?php endif; ?>
	
</form>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('.zoorder-order').change(function(){
		$('.zoorder-ordertype').val($('.zoorder-order:selected').val());
	});
});
</script>