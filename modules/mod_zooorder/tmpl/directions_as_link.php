<?php
/**
* @package		ZOOorder
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<form action="<?php echo JURI::getInstance()->current(); ?>" method="GET">
	<?php if( count( $elements) ): ?>
		
		<?php foreach($url_params as $key => $param): ?>
			<input type="hidden" name="<?php echo $key;?>" value="<?php echo $param;?>" />
		<?php endforeach; ?>
	
		<select name="order">
			<?php foreach( $elements as $element ): ?>
			<option value="<?php echo $element['element']->identifier; ?>" <?php echo (JRequest::getVar('order', '') == $element['element']->identifier) ? 'selected="selected"' : ''; ?>>
				<?php echo $element['label']; ?>
			</option>
			<?php endforeach; ?>	
		</select>
		<span class="direction">
			<a rel="nofollow" class="asc<?php echo (JRequest::getVar('direction', '') == 'asc') ? ' selected' : ''; ?>" href="javascript:void(0)" title="<?php echo JText::_('Sort').' '.JText::_('PLG_ZOOORDER_ASCENDING'); ?>"><span><?php echo JText::_('PLG_ZOOORDER_ASCENDING'); ?></span></a>
			<a rel="nofollow" class="desc<?php echo (JRequest::getVar('direction', '') == 'desc') ? ' selected' : ''; ?>" href="javascript:void(0)" title="<?php echo JText::_('Sort').' '.JText::_('PLG_ZOOORDER_DESCENDING'); ?>"><span><?php echo JText::_('PLG_ZOOORDER_DESCENDING'); ?></span></a>
		</span>
		<select name="direction" style="visibility:hidden;width: 0;">
			<option value="asc" <?php echo (JRequest::getVar('direction', '') == 'asc') ? 'selected="selected"' : ''; ?>><?php echo JText::_('PLG_ZOOORDER_ASCENDING'); ?></option>
			<option value="desc" <?php echo (JRequest::getVar('direction', '') == 'desc') ? 'selected="selected"' : ''; ?>><?php echo JText::_('PLG_ZOOORDER_DESCENDING'); ?></option>		
		</select>

		<input type="submit" value="<?php echo JText::_('PLG_ZOOORDER_SORT'); ?>"/>
	
	<?php else: ?>
		<?php echo JText::_('PLG_ZOOORDER_MOD_NO_ELEMENTS'); ?>
	<?php endif; ?>
	
</form>

<script type="text/javascript">
/* <![CDATA[ */
jQuery(document).ready(function() {

	jQuery('.direction a').click(function() {
		
		var direction = jQuery(this).attr('class');
		jQuery('select[name="direction"]').val(direction);

		jQuery('.direction a').removeClass('selected');
		jQuery(this).addClass('selected');

	});

});
/* ]]> */
</script>
