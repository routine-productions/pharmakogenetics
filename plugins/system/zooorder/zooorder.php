<?php
/**
* @package		ZOOorder
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

class plgSystemZooorder extends JPlugin
{ 
	public $joomla;
	public $app;

	/**
	 * onAfterInitialise handler
	 *
	 * Adds ZOO event listeners
	 *
	 * @access	public
	 * @return null
	 */
	public function onAfterInitialise()
	{
		// Get Joomla instances
		$this->joomla = JFactory::getApplication();
		$jlang = JFactory::getLanguage();
		
		// load default and current language
		$jlang->load('plg_system_zooorder', JPATH_ADMINISTRATOR, 'en-GB', true);
		$jlang->load('plg_system_zooorder', JPATH_ADMINISTRATOR, null, true);

		// check dependences
		if (!defined('ZLFW_DEPENDENCIES_CHECK_OK')){
			$this->checkDependencies();
			return; // abort
		}

		// Get the ZOO App instance
		$this->app = App::getInstance('zoo');
		
		// register plugin path
		if ( $path = $this->app->path->path( 'root:plugins/system/zooorder/zooorder' ) ) {
			$this->app->path->register($path, 'zooorder');
		}
		
		// register fields path
		if ( $path = $this->app->path->path( 'zooorder:fields' ) ) {
			$this->app->path->register($path, 'zlfields');
		}
		
		// register helpers
		if ( $path = $this->app->path->path( 'zooorder:helpers/' ) ) {
			$this->app->path->register($path, 'helpers');
			$this->app->loader->register('ZOXmlparamsHelper', 'helpers:zoxmlparams.php');
		}
		
		// register events
		$this->app->event->dispatcher->connect('element:configform', array($this, 'configForm'));
		$this->app->event->dispatcher->connect('layout:init', array($this, 'initTypeLayouts'));
		$this->app->event->dispatcher->connect('category:init', array($this, 'addItemOrder'));
		$this->app->event->dispatcher->connect('application:init', array($this, 'addItemOrder'));
	}

	/**
	 * Add parameter to the elements config form
	 */
	public function configForm($event, $arguments = array())
	{
		$element = $event->getSubject();
		$type	 = $element->getType();
		$config  = $event['form'];
		$files	 = array();

		// add XML params path
		$config->addElementPath($this->app->path->path('zooorder:fields'));
		$config->addElementPath($this->app->path->path('zlfw:fields'));

		// if type has value, we are in Backend and the module path is correct
		if (!empty($type) && JFactory::getApplication()->isAdmin()) {	
				
			// order
			if (strstr(JRequest::getVar('path'), 'ordering') || strstr(JRequest::getVar('path'), 'zooorder')) {
				$files[] = $this->app->path->path('zooorder:params/ordering.xml');

				// apply new params
				$this->app->zoxmlparams->addElementParams($config, $files);
			}
		}
	}
	
	/*
		Function: initTypeLayouts
			Callback function for the zoo layouts

		Returns:
			void
	*/
	public function initTypeLayouts($event)
	{
		$extensions = (array) $event->getReturnValue();
		
		// clean all ZOOorder layout references
		$newextensions = array();
		foreach ($extensions as $ext) {
			if (stripos($ext['name'],'zooorder') === false) {
				$newextensions[] = $ext;
			}
		}
		
		// add new ones
		$newextensions[] = array('name' => 'ZOOorder', 'path' => $this->app->path->path('zooorder:'), 'type' => 'plugin');
		
		$event->setReturnValue($newextensions);
	}
	
	public function addItemOrder($event)
	{	
		if(JFactory::getApplication()->isSite())
		{
			$category = $event->getSubject();
			
			$this->order_application = JRequest::getVar('application', null);
			
			if ($category instanceof Application) {
				$this->application = $category->id;
			} else {
				$this->application = $category->getApplication()->id;
			}
			
			$this->getFromSession();
			
			if(JRequest::getVar('order', null))
			{
				$this->saveToSession();

				$order = JRequest::getVar('order', '_itemname');
				$dir = strtoupper(JRequest::getVar('direction', 'DESC'));
				
				$item_order = array();
				$item_order[] = $order;
				if($dir == 'DESC')
				{
					$item_order[] = '_reversed';
				}

				$ordertype = JRequest::getVar('ordertype', $this->app->session->get('com.zoo.zooorder.ordertype', false));
				if ($ordertype) 
				{
					$this->app->session->set('com.zoo.zooorder.ordertype', $ordertype);
					$item_order[] = '_alphanumeric';
				}
				
				$category->params->set('global.config.item_order', $item_order);
				$category->params->set('config.item_order', $item_order);
			}
		}
	}
	
	/**
	 * When using pagination, the order params are not stored, so let's use session instead
	 */
	protected function getFromSession()
	{
		$order = $this->app->system->application->getUserState('zoo.zooorder.app_'.$this->application.'._order', null);
		$direction = $this->app->system->application->getUserState('zoo.zooorder.app_'.$this->application.'._dir', 'DESC');

		if($order && !JRequest::getVar('order', false))
		{
			JRequest::setVar('order', $order);
			JRequest::setVar('direction', $direction);
		}
	}
	
	/**
	 * When using pagination, the order params are not stored, so let's use session instead
	 */
	protected function saveToSession()
	{
		if ($this->order_application) {
			$order = JRequest::getVar('order', false);
			$direction = JRequest::getVar('direction', 'DESC');
			
			if($order)
			{
				$this->app->system->application->setUserState('zoo.zooorder.app_'.$this->order_application.'._order', $order);
				$this->app->system->application->setUserState('zoo.zooorder.app_'.$this->order_application.'._dir', $direction);
			}
		}
	}
	
	/*
	 *  checkDependencies
	 */
	public function checkDependencies()
	{
		if($this->joomla->isAdmin())
		{
			// if ZLFW not enabled
			if(!JPluginHelper::isEnabled('system', 'zlframework') || !JComponentHelper::getComponent('com_zoo', true)->enabled) {
				$this->joomla->enqueueMessage(JText::_('PLG_ZOOORDER_ZLFW_MISSING'), 'notice');
			} else {
				// load zoo
				require_once(JPATH_ADMINISTRATOR.'/components/com_zoo/config.php');

				// fix plugins ordering
				$zoo = App::getInstance('zoo');
				$zoo->loader->register('ZlfwHelper', 'root:plugins/system/zlframework/zlframework/helpers/zlfwhelper.php');
				$zoo->zlfw->checkPluginOrder('zooorder');
			}
		}
	}
}