<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

	// load assets
	$this->app->document->addScript('elements:downloadpro/tmpl/submission/default/_sublayouts/_default/submission.min.js');

	// init vars
	$upload = $this->get('file');

	// is uploaded file
	$upload = is_array($upload) ? '' : $upload;

	if (!empty($upload)) {
		$upload = basename($upload);
	}
	
?>

<div class="download-element">

	<div class="download-select">

		<div class="upload">
			<input class="filename"  type="text" id="filename<?php echo $this->identifier; ?>" readonly="readonly" />
			<div class="button-container">
				<button class="button-grey search" type="button"><?php echo JText::_('PLG_ZLFRAMEWORK_SEARCH'); ?></button>
				<input type="file" name="elements_<?php echo $this->identifier; ?>[]" />
			</div>
		</div>

		<input type="hidden" class="upload" name="<?php echo $this->getControlName('upload'); ?>" value="<?php echo ($upload ? $this->index() : ''); ?>" />

    </div>

    <div class="download-preview hidden">
        <span class="preview"><?php echo $upload; ?></span>
        <span class="download-cancel" title="<?php echo JText::_('PLG_ZLFRAMEWORK_REMOVE'); ?>"></span>
    </div>
	
	<script type="text/javascript">
	jQuery(function($) {
		$('#<?php echo $this->identifier; ?>').DownloadProSubmissionDefault();
	});
	</script>

</div>