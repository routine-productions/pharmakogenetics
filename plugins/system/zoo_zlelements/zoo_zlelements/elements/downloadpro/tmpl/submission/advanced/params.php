<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

	return 
	'{"fields": {
		"_mode_advanced_info":{
			"type":"info",
			"specific": {
				"text":"PLG_ZLFRAMEWORK_FLP_ADVANCED_LAYOUT_WARNING"
			}
		}
	},
	"control": "layout"}';