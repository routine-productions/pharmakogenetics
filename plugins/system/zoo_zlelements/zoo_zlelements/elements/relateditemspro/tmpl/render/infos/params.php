<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');	
	
	return 
	'{"fields": {

		"layout_wrapper":{
			"type": "fieldset",
			"min_count":"1",
			"fields": {

				"_infos":{
					"type": "text",
					"label": "PLG_ZLELEMENTS_RI_INFO_TO_SHOW",
					"help": "PLG_ZLELEMENTS_RI_INFO_TO_SHOW_DESC"
				},

				"relation_options": {
					"type": "subfield",
					"path":"elements:'.$element->getElementType().'\/params\/relation.php",
					"adjust_ctrl":{
						"pattern":'.json_encode('/\[layout\]/').',
						"replacement":"[specific]"
					}
				}

			}
		}

	}}';
		