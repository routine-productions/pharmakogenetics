<?php
/**
* @package		ZL FrameWork
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

	return 
	'{"fields": {

		"layout_separator":{
			"type": "separator",
			"text": "PLG_ZLFRAMEWORK_LAYOUT",
			"big":"1"
		},
		"relation_options": {
			"type": "subfield",
			"path":"elements:'.$element->getElementType().'\/params\/relation.php",
			"adjust_ctrl":{
				"pattern":'.json_encode('/\[layout\]\[widgetkit\]\[settings\]/').',
				"replacement":"[specific]"
			}
		},

		"widget_separator":{
			"type": "separator",
			"text": "Widget",
			"big":"1"
		},
		"_titlelayout":{
			"type": "itemLayoutList",
			"label": "PLG_ZLELEMENTS_RI_WDKT_TITLE_LAYOUT",
			"specific":{
				"options": {
					"PLG_ZLELEMENTS_RI_WDKT_ITEM_NAME":""
				}
			}
		},
		"widget_settings": {
			"type":"subfield",
			"path":"elements:pro\/tmpl\/render\/widgetkit\/slideshow\/list\/settings.php"
		}

	},
	"control": "settings"}';