<?php
/*
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

	$extra_infos = $params->find('layout._infos', '') != '' ? $params->find('layout._infos') : $this->config->find('specific._extra_info', '');
	$extra_infos = explode(",", $extra_infos);

	foreach($extra_infos as &$ex_i){
		$ex_i = strtolower($ex_i);
	}

	$saved_infos = $this->_item->params->get('relateditemspro.');

	$result = array();
	foreach($saved_infos as $label => $info){
		$result = '<strong>'.$label.'</strong>'.$info;
	}

	$separator = $params->find('separator._by') == 'custom' ? $params->find('separator._by_custom') : $params->find('separator._by');

	echo $this->app->zlfw->applySeparators($separator, $result, $params->find('separator._class'), $params->find('separator._fixhtml'));
?>