<?php
/*
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

$saved_infos = $item->params->get('relateditemspro.');
?>
<div class="extra-info">
	<?php foreach($saved_infos as $label => $info): ?>
		<?php 
		$check = strtolower($label);
		if(!empty($info) && ((!count($infos) || in_array($check, $infos)))): ?>
		<div class="extra-info">
			<strong><?php echo $label;?>:</strong>&nbsp;<?php echo $info;?>
		</div>
		<?php endif;?>
	<?php endforeach;?>
</div>