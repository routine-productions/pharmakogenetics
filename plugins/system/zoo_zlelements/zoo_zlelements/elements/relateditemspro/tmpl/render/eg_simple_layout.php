<?php
/*
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');
	
	$result = array();
	$result['result'] = $this->getRelatedItems(true, $params);

	$result = $this->_filterItems($result, $params->get('filter'));
	$result = $this->_orderItems($result, $params->get('order'));
?>

<!-- create output -->
<ul>
	<?php foreach($result['result'] as $item) : ?>

		<li>
			<?php echo $this->_renderItem($item); ?>
		</li>
		
	<?php endforeach; ?>
</ul>