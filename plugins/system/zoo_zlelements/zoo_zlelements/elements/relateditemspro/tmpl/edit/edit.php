<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// sending 'elm_id' & 'element' to work both on Admin and Submission
$ajax_url = $this->app->link(array('controller' => 'zlframework', 'task' => 'callelement', 'format' => 'raw', 'type' => $this->_item->getType()->id, 'item_id' => $this->_item->id, 'elm_id' => $this->identifier), false);

// set author filter
$authors = $this->config->find('specific._authored') ? $this->app->user->get()->id : null;

// check if submission
$is_submission = $this->app->request->getString('view') == 'submission' ? 1 : 0;

?>

<div id="<?php echo $this->identifier; ?>" class="select-relateditems pro zl">

	<ul>
	<?php foreach ($data as $item) : $app = $item->getApplication(); ?>

		<li data-id="<?php echo $item->id ?>">
			<div>
				<div class="item-name"><?php echo $this->app->zlstring->truncate($item->name, 55, '...'); ?></div>
				<span class="item-tools">
					<?php if (!$is_submission) : ?>
					<span class="zl-btn-small edit action" title="<?php echo JText::_('PLG_ZLELEMENTS_RI_EDIT_ITEM'); ?>" data-url="index.php?option=com_zoo&controller=item&task=edit&changeapp=<?php echo $app->id; ?>&cid[]=<?php echo $item->id; ?>"></span>
					<?php endif; ?>
					<?php if ($editable) : ?>
					<span class="zl-btn-small delete action" title="<?php echo JText::_('PLG_ZLELEMENTS_RI_DELETE_ITEM'); ?>"></span>
					<?php endif; ?>
					<span class="zl-btn-small sort action" title="<?php echo JText::_('PLG_ZLELEMENTS_RI_SORT_ITEM'); ?>"></span>
				</span>
				<div class="item-info">
					<div class="item-type"><span class="zl-btn-small type"></span><?php echo ucfirst($item->getType()->name); ?></div>
					<div class="item-app"><span class="zl-btn-small app"></span><?php echo $app->name; ?></div>
				</div>
				<input type="hidden" name="<?php echo $this->getControlName('item', true); ?>" value="<?php echo $item->id; ?>"/>
				
				<?php if($this->config->find('specific._extra_info', '')) : ?>
				<div class="more-options">
					<div class="trigger">
						<div>
							<div class="advanced button hide"><?php echo JText::_('PLG_ZLELEMENTS_RI_EXTRA_INFO'); ?></div>
							<div class="advanced button"><?php echo JText::_('PLG_ZLELEMENTS_RI_EXTRA_INFO'); ?></div>
						</div>
					</div>

					<div class="advanced options">
						<?php 
						// Get extra information for the relationship
						$extra_info = explode(",", $this->config->find('specific._extra_info', ''));
						foreach($extra_info as $key):	
							$key = trim($key);
						?>
						<div class="row short">
							<input type="text" name="<?php echo $this->getControlName("param_$key", true); ?>" value="<?php echo $item->params->get('relateditemspro.'.$key); ?>"/>
							<label><?php echo $key; ?></label>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</li>

	<?php endforeach; ?>
	</ul>
	
</div>

<script type="text/javascript">
	jQuery(function($) {
		$('#<?php echo $this->identifier; ?>').ElementRelatedItemsPro({
			msgEditItem: '<?php echo JText::_('PLG_ZLELEMENTS_RI_EDIT_ITEM'); ?>', 
			msgDeleteItem: '<?php echo JText::_('PLG_ZLELEMENTS_RI_DELETE_ITEM'); ?>', 
			msgSortItem: '<?php echo JText::_('PLG_ZLELEMENTS_RI_SORT_ITEM'); ?>', 
			msgAddItem: '<?php echo JText::_('PLG_ZLELEMENTS_RI_ADD_ITEM'); ?>', 
			title: '<?php echo htmlspecialchars($this->config->get('name'), ENT_QUOTES) ?>', 
			controlName: '<?php echo $this->getControlName('item', true); ?>', 
			editable: <?php echo $editable ?>,
			apps: '<?php echo implode(',', (array)$this->config->find('application._chosenapps')) ?>',
			types: '<?php echo implode(',', (array)$this->config->find('application._chosentypes')) ?>',
			authors: '<?php echo $authors ?>',
			chosenElms: '<?php echo json_encode($chosenElms) ?>',
			ajax_url: '<?php echo $ajax_url ?>',
			is_submission: <?php echo $is_submission; ?>
		});
	});
</script>