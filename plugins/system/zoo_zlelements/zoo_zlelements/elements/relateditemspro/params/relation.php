<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

	$app = App::getInstance('zoo');
	
	$element_list = array(); $element_list['None'] = ''; $element_list = array_merge( $element_list, $app->zlfield->elementsList($element->config->find('application._chosenapps'), 'relateditemspro', $element->config->find('application._chosentypes')) );

	return 
	'{
		"_rlayout":{
			"type": "itemLayoutList",
			"label": "PLG_ZLFRAMEWORK_RELATED_LAYOUT",
			"help": "OVERRIDE_DEFAULT_OUTPUT",
			"dependents": "itemname-options > NONE",
			"specific":{
				"options": {
					"Item Name":"",
					"Raw URL":"raw_url"
				}
			}
		},
		"itemname-options":{
			"type": "wrapper",
			"fields": {
				"_link_to_item":{
					"type": "checkbox",
					"label": "PLG_ZLFRAMEWORK_LINK_TO_ITEM",
					"help": "PLG_ZLELEMENTS_LINK_TO_ITEM",
					"default": "0",
					"specific":{
						"label": "JYES"
					},
					"dependents": "_link_text > 1"
				},
				"_link_text":{
					"type": "text",
					"label": "Item Link Text"
				}
			}
		},
		"Separator":{
			"type": "separator",
			"text": "PLG_ZLELEMENTS_RI_SUBRELATIONS"
		},
		"_subrelation":{
			"type": "select",
			"label": "PLG_ZLELEMENTS_RI_SUBRELATED_ELEMENT",
			"help": "PLG_ZLELEMENTS_RI_SUBRELATIONS_DESC",
			"dependents": "_excludeitself !> NONE",
			"specific":{
				"options": '.json_encode($element_list).'
			}
		},
		"_excludeitself":{
			"type": "checkbox",
			"label": "PLG_ZLELEMENTS_RI_EXCLUDE_IT_SELF",
			"help": "PLG_ZLELEMENTS_RI_EXCLUDE_IT_SELF_DESC",
			"default": "0",
			"specific":{
				"label": "JYES"
			}
		}
	}';