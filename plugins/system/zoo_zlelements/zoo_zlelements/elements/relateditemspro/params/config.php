<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

	$app = App::getInstance('zoo');

	return 
	'{
		"_editable":{
			"type": "radio",
			"label": "PLG_ZLELEMENTS_RI_EDIT",
			"help": "PLG_ZLELEMENTS_RI_EDIT_DESC",
			"default": "1"
		},
		"_extra_info":{
			"type": "text",
			"label": "PLG_ZLELEMENTS_RI_EXTRA_INFO",
			"help": "PLG_ZLELEMENTS_RI_EXTRA_INFO_DESC",
			"default": ""
		},
		"_authored":{
			"type": "checkbox",
			"label": "PLG_ZLELEMENTS_RI_AUTHORED",
			"help": "PLG_ZLELEMENTS_RI_AUTHORED_DESC",
			"default": "",
			"specific":{
				"label":"PLG_ZLFRAMEWORK_ENABLE"
			}
		}
	}';
			
	/*  "_integrations_wrapper":{
			"type": "wrapper",
			"min_count":"2",
			"fields": {
				"_integration_sep":{
					"type": "separator",
					"text": "Integrations"
				},
				"_zaksubs_integration":{
					"type": "checkbox",
					"label": "ZOOaksubs",
					"renderif":{
						"zooaksubs":"1"
					},
					"help":"ZAKS_RI_PACKAGE_INF",
					"specific":{
						"label":"ZAKS_RI_PACKAGE"
					}
				}
			}
	} */