<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

	// get element from parent parameter form
	$element = $parent->element;
	$config  = $element->config;

	// init var
	$node_atr = (array)$node->attributes();
	$node_atr = $node_atr['@attributes'];
	
	// set arguments
	$ajaxargs  = array('node' => $node_atr);
	$arguments = array('node' => $node_atr);
	$class	   = $node->attributes()->class;

	// if in element params, set it's arguments
	$ajaxargs['element_type'] = $element->getElementType();
	$ajaxargs['element_id']   = $element->identifier;
	$arguments['element'] = $element;

	// get apps filter
	$apps = $this->app->zlfw->getApplications($config->find('application._chosenapps', array()), true);
	foreach ($apps as &$app) {
		$app = $app->id;
	}

	// get types filter
	$types = $config->find('application._chosentypes', array());

	// set json
	$json = '{"fields": {

		"item_dates": {
			"type":"subfield",
			"path":"zlfield:json/itemorder.json.php",
			"arguments":{
				"params":{
					"apps":'.json_encode($apps).',
					"types":'.json_encode($types).'
				}
			},
			"control":"order"
		}

	}}';

	// set ctrl
	$ctrl = "{$control_name}".($node->attributes()->addctrl ? "[{$node->attributes()->addctrl}]" : '');

	// set toggle hidden label
	$thl = $node->attributes()->togglelabel ? $node->attributes()->togglelabel : $node->attributes()->label;

	// render
	echo $this->app->zlfield->render(array($json, $ctrl, array(), '', false, $arguments), $node->attributes()->toggle, JText::_($thl), $ajaxargs, $class);