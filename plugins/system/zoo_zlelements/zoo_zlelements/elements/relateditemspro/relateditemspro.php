<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

/*
	Class: ElementRelatedItems
		The related items element class
*/
class ElementRelatedItemsPro extends ElementPro implements iSubmittable {

	protected $_related_items;
	
	/*
	   Function: Constructor
	*/
	public function __construct() {

		// call parent constructor
		parent::__construct();

		// set callbacks
		$this->registerCallback('addtoremovalqueue');

		// register events
		$this->app->event->dispatcher->connect('element:afterdisplay', array($this, 'afterDisplay'));
		$this->app->event->dispatcher->connect('item:saved', array($this, 'biRelate'));

		// init vars
		$this->db = $this->app->database;
	}

	/*
		Function: afterDisplay
			Change the element layout after it has been displayed

		Parameters:
			$event - object
	*/
	public static function afterDisplay($event)
	{
		$event['html'] = str_replace('class="element element-relateditemspro', 'class="element element-relateditemspro element-relateditems', $event['html']);
	}

	/*
		Function: biRelate
			Triggered on item:saved event for RI Pro Bi-relation feature
	*/
	public function biRelate($event) {

		// init vars
		$item = $event->getSubject();
		$elements = $item->getElements();
		
		// birelate the items
		foreach($elements as $element){
			if($element->getElementType() == 'relateditemspro'){
			
				// filter the elements and get the relations
				$chosenElms = $element->config->find('application._chosenelms', array());
				$ritems = $element->getRelatedItems(false); // get related, even unpublished, items
				
				// associate actual $item to it's relations
				if (!empty($ritems) && !empty($chosenElms)) foreach ($ritems as $ritem){
					foreach ($chosenElms as $relement_id) {
						if ($relement = $ritem->getElement($relement_id)) {
							// bi-related element found on item
							$relement->addRelation($item->id);
						}
					}					
				}
				
				// in same step search items marked to be Removed and remove
				foreach ($chosenElms as $relement_id) {
					$query = 'DELETE FROM #__zoo_relateditemsproxref'
							.' WHERE remove = 1' 
							.' AND ritem_id = '.$item->id
							.' AND element_id='.$this->db->Quote($relement_id);
					$this->db->query($query);
				}
			}
		}
		
		return null;
	}

	/*
		Function: hasValue
			Checks if the element's value is set.

	   Parameters:
			$params - render parameter

		Returns:
			Boolean - true, on success
	*/		
	public function hasValue($params = array())
	{
		if ($params->find('layout._layout', '') == 'infos.php') return true;
		
		$value = $this->getRenderedValues( $this->app->data->create($params) );
		return !empty($value['result']);
	}
	
	/*
		Function: getRenderedValues
			renders the element content

		Returns:
			array
	*/
	protected $_rendered_values = array();
	public function getRenderedValues($params=array(), $mode='') 
	{
		// create a unique hash for this element position
		$hash = md5(serialize(array(
			$mode,
			$params->get('element').$params->get('_layout'),
			$params->get('_position').$params->get('_index')
		)));

		// check for value, if not exist render it
		if (!array_key_exists($hash, $this->_rendered_values))
		{
			// of limit 0, return no results
			if ($params->find('filter._limit') == '0') return array();
		
			// init vars
			$exc_it_self = $params->find('specific._excludeitself');
			$rl = $params->find('specific._rlayout');
			$li = $params->find('specific._link_to_item');
			$lt = $params->find('specific._link_text');

			$result = array();
			$result['result'] = $this->getRelatedItems(true, $params);

			// Sub Relations
			if ($subrelation = $params->find('specific._subrelation', 0))
			{
				$subitems = array();
				foreach($result['result'] as $item) if ($subelement = $item->getElement($subrelation)){
					foreach($subelement->getRelatedItems(true, $params) as $id => $subitem) {
						if (empty($exc_it_self) || $this->_item->id != $id){
							$subitems[$id] = $subitem;
							// we can't use array_merge because will reset the array index
				}	}	}
				$result['result'] = $subitems;
			}
			
			$result = $this->_orderItems($result, $params->get('order')); // ordering goes before filtering
			$result = $this->_filterItems($result, $params->get('filter'));
		
			// create output
			$i = 0;
			foreach($result['result'] as $e => $item)
			{
				switch (preg_replace('/\..+/', '', $mode)) // return the mode without any post . text
				{
					// WK Slideshow
					case 'slideshow':
						$tl = $params->find('layout.widgetkit.settings._titlelayout');
						$nl = $params->find('layout.widgetkit.settings._navlayout');
						$cl = $params->find('layout.widgetkit.settings._caplayout');
						$wkitem = array();
						$wkitem['caption'] 		= $this->_renderItem($item, $i, $params, $cl);
						$wkitem['navigation'] 	= $this->_renderItem($item, $i, $params, $nl);
						$wkitem['title'] 		= $this->_renderItem($item, $i, $params, $tl);
						$wkitem['content'] 		= $this->_renderItem($item, $i, $params, $rl, $li, $lt);
						$result['result'][$e] = $wkitem;
						break;
						
					// WK Slideset // WK Accordion
					case 'slideset': case 'accordion':
						$tl = $params->find('layout.widgetkit.settings._titlelayout');
						$wkitem = array();
						$wkitem['set'] 			= '';
						$wkitem['title'] 		= $this->_renderItem($item, $i, $params, $tl);
						$wkitem['content'] 		= $this->_renderItem($item, $i, $params, $rl, $li, $lt);
						$result['result'][$e] = $wkitem;
						break;
						
					default:
						$result['result'][$e] = $this->_renderItem($item, $i, $params, $rl, $li, $lt);
						break;
				}
				$i++;
			}
			
			$this->_rendered_values[$hash] = $result;
		}
		
		return $this->_rendered_values[$hash];
	}
	
	protected function _renderItem($item, $i=0, $params=array(), $layout=null, $link_to_item=null, $link_text=null)
	{	
		// as we support crossapp feature, let's change app path on each item
		$renderer = $this->app->renderer->create('item')->addPath( array($item->getApplication()->getTemplate()->getPath(), $this->app->path->path('component.site:')) );
		
		$path   = 'item';
		$prefix = 'item.';
		$type   = $item->getType()->id;
		if ($renderer->pathExists($path.DIRECTORY_SEPARATOR.$type)) {
			$path   .= DIRECTORY_SEPARATOR.$type;
			$prefix .= $type.'.';
		}

		// set view object for teaser layout workaround
		if(empty($view)){
			$view = new AppView( array('name' => 'item'));
			$view->params = $item->getApplication()->getParams('site');
		}
		
		if (in_array($layout, $renderer->getLayouts($path))) {
			return $renderer->render($prefix.$layout, compact('item', 'i', 'params', 'view'));
		} elseif ($layout == 'raw_url' && $item->getState()) {
			return $this->app->route->item($item);
		} elseif ($link_to_item && $item->getState()) {
			$linktext = $link_text ? $link_text : $item->name;
			return '<a href="'.$this->app->route->item($item).'" title="'.$linktext.'">'.$linktext.'</a>';
		} else {
			return $item->name;
		}
		return '';
	}

	public function getRelatedItems($published = true, $params = null)
	{
		if ($this->_related_items == null)
		{
			// init vars
			$table = $this->app->table->item;
			$this->_related_items = array();
			$related_items = array();
			$items = array();
			$item_id = $this->_item->id ? $this->_item->id : 0;
			
			// get items from DB
			$query = 'SELECT ritem_id, params'
					.' FROM #__zoo_relateditemsproxref'
					.' WHERE item_id = '.$item_id
					.' AND element_id='.$this->db->Quote($this->identifier)
					.' ORDER BY id ASC';
			$itemsData = $this->db->setQuery($query)->loadAssocList();

			// build items array
			if (empty($itemsData)) {
				// if no DB result get from Item data
				foreach ($this->get('item', array()) as $id) {
					$item = array('ritem_id' => $id, 'params' => '{}');
					$items[$id] = $item;
				}

			} else {
				foreach ($itemsData as $item) {
					$items[$item['ritem_id']] = $item;
				}
			}

			// save new array back
			$itemsData = $items;

			// check if items have already been retrieved
			foreach ($items as $id => $item) {
				if ($table->has($id)) {
					$related_items[$id] = $table->get($id);
					unset($items[$id]);
				}
			}

			// get items ids
			$items_id = array();
			foreach ($items as $id => $item) {
				$items_id[] = $id;
			}

			if (!empty($items)) 
			{	
				// get dates
				$date = $this->app->date->create();
				$now  = $this->db->Quote($date->toSQL());
				$null = $this->db->Quote($this->db->getNullDate());
				$items_string = implode(', ', $items_id);
				$conditions = $table->key.' IN ('.$items_string.')'
							.($published ? ' AND state = 1'
							.' AND '.$this->app->user->getDBAccessString()
							.' AND (publish_up = '.$null.' OR publish_up <= '.$now.')'
							.' AND (publish_down = '.$null.' OR publish_down >= '.$now.')' : '');
				$order = 'FIELD('.$table->key.','.$items_string.')';
				$related_items += $table->all(compact('conditions', 'order'));
			}

			foreach ($itemsData as $id => $item) {
				if (isset($related_items[$id])) {
					$this->_related_items[$id] = $this->_parseParameters($related_items[$id], $item['params']);
				}
			}
		}

		return $this->_related_items;
	}

	/**
	 * Add parameters of the related to the item parameters
	 */
	protected function _parseParameters($item, $params)
	{
		$params = json_decode($params, true);
		if($item && $params && count($params)){
			foreach($params as $key => $value){
				$item->params->set("relateditemspro.$key", $value);
			}
		}
		
		return $item;
	}
	
	/*
	   Function: _orderItems
		   Order the Items before render

	   Returns:
		   String - array
	*/
	protected function _orderItems($result, $order)
	{
		// if string, try to convert ordering
		if (is_string($order)) {
			$order = $this->app->itemorder->convert($order);
		}

		$items = (array) $result['result'];
		$order = (array) $order;
		$sorted = array();
		$reversed = false;

		// remove empty values
		$order = array_filter($order);

		// if random return immediately
		if (in_array('_random', $order)) {
			shuffle($items);
			$result['result'] = $items;
			return $result;
		}

		// get order dir
		if (($index = array_search('_reversed', $order)) !== false) {
			$reversed = true;
			unset($order[$index]);
		} else {
			$reversed = false;
		}

		// order by default
		if (empty($order)) {
			$result['result'] = $reversed ? array_reverse($items, true) : $items;
			return $result;
		}

		// if there is a none core element present, ordering will only take place for those elements
		if (count($order) > 1) {
			$order = array_filter($order, create_function('$a', 'return strpos($a, "_item") === false;'));
		}

		if (!empty($order)) {

			// get sorting values
			foreach ($items as $item) {
				foreach ($order as $identifier) {
					if ($element = $item->getElement($identifier)) {
						$sorted[$item->id] = strpos($identifier, '_item') === 0 ? $item->{str_replace('_item', '', $identifier)} : $element->getSearchData();
						break;
					}
				}
			}

			// do the actual sorting
			$reversed ? arsort($sorted) : asort($sorted);

			// fill the result array
			foreach (array_keys($sorted) as $id) {
				if (isset($items[$id])) {
					$sorted[$id] = $items[$id];
				}
			}

			// attach unsorted items
			$sorted += array_diff_key($items, $sorted);

		// no sort order provided
		} else {
			$sorted = $items;
		}

		$result['result'] = $sorted;
		return $result;
	}
	
	/*
	   Function: _filterItems
		   Filter the Items before render

	   Returns:
		   String - array
	*/
	protected function _filterItems($result, $filter)
	{
		$items = (array) $result['result'];
		$filter = (array) $filter;
		$filtered = array();

		// remove empty values
		$filter = array_filter($filter);
		
		if (!empty($filter)) {
		
			// filter by type
			if (!empty($filter['_types'])) foreach ($items as $item) {
				if (in_array($item->getType()->id,  $filter['_types'])) {
					$filtered[$item->id] = $item;
				}
			} else {$filtered = $items;}
			
			$offset = array_key_exists('_offset', $filter) ? $filter['_offset'] : 0;
			$limit  = array_key_exists('_limit', $filter) ? $filter['_limit'] : null;
			
			// set offset/limit
			$filtered = array_slice($filtered, $offset, $limit, true);
			$result['report']['limited'] = true;
		
		// no sort order provided
		} else {
			$filtered = $items;
		}

		$result['result'] = $filtered;
		return $result;
	}
	
	/*
	   Function: getApplications
	   Returns:
		   array
	*/	
	public function getApplications() {
		$apps = $this->config->find('application._chosenapps', 0);
		return  $this->app->zlfw->getAppsObject($apps, true); // if empty will return All apps
	}
	
	/*
	   Function: edit
		   Renders the edit form field.

	   Returns:
		   String - html
	*/
	public function edit() {
		return $this->_edit(false);
	}

	protected function _edit($published = true) {
	
		// cleans any cancelled remove action
		$this->_cleanRemovalQueue();

		// render layout
		if ($layout = $this->getLayout('edit/edit.php')) {
			return $this->renderLayout($layout,
				array(
					'editable' => $this->config->find('specific._editable', 1),
					'data' => $this->getRelatedItems($published),
					'chosenElms' => $this->config->find('application._chosenelms', array())
				)
			);
		}
	}
	
	/*
		Function: renderSubmission
			Renders the element in submission.

	   Parameters:
			$params - submission parameters

		Returns:
			String - html
	*/
	public function renderSubmission($params = array())
	{
		// load assets
		$this->app->document->addStylesheet('zlfw:assets/css/zl_ui.css');
		
		return $this->_edit();
	}

	/*
		Function: validateSubmission
			Validates the submitted element

	   Parameters:
			$value  - AppData value
			$params - AppData submission parameters

		Returns:
			Array - cleaned value
	*/
	public function validateSubmission($value, $params) {

		$options     = array('required' => $params->get('required'));
		$messages    = array('required' => 'Please select at least one related item.');

		$items = (array) $this->app->validator
				->create('foreach', null, $options, $messages)
				->clean($value->get('item'));
		
		return array('item' => $items);
	}

	/*
		Function: loadAssets
			Load elements css/js assets.

		Returns:
			Void
	*/
	public function loadAssets() {

		// load ZLUX assets
		$this->app->zlfw->zlux->loadMainAssets();

		// load RI Pro assets
		$this->app->document->addStylesheet('elements:relateditemspro/assets/relateditemspro.css');
		$this->app->document->addScript('elements:relateditemspro/assets/relateditemspro.min.js');
	}
	
	/*
		Function: addToRemovalQueue
			For ajax call - it adds to the DB the item to be Removed from relation
	*/
	public function addToRemovalQueue($id = null, $chosenElms = '[]') {
		
		if(isset($id)) {
			foreach(json_decode($chosenElms) as $element_id) {
				$query = "UPDATE `#__zoo_relateditemsproxref`"
						." SET remove = 1"
						." WHERE item_id = ".$id
						." AND ritem_id = ".$this->_item->id
						." AND element_id = ".$this->db->Quote($element_id);
				$this->db->query($query);
			}
			
			return json_encode(array(
				'item_id' => $id,
				'ritem_id' => $this->_item->id,
				'element_id' => json_decode($chosenElms)
			));
		}
		return false;
	}
	
	/*
		Function: _cleanRemovalQueue
			Clean from DB any queue related to this item because the user didn't confirm the actions
	*/
	protected function _cleanRemovalQueue()
	{
		$id = $this->_item->id ? $this->_item->id : 0;
		$chosenElms = $this->config->find('application._chosenelms', array());
		
		foreach($chosenElms as $element) {
			$query = "UPDATE `#__zoo_relateditemsproxref`"
					." SET remove = 0"
					." WHERE remove = 1" 
					." AND ritem_id = ".(int)$id
					." AND element_id = ".$this->db->Quote($element);
			$this->db->query($query);
		}
		
		return true;
	}
	
	/*
		Function: addRelation
			Used for Bidirectional relations
	*/
	public function addRelation($ritem = null)
	{	
		// Debug
		if (JDEBUG){
			JFactory::getApplication()->enqueueMessage( 'Birelated to Item "'.$this->_item->name.'" with ID '.$this->_item->id );
		}

		// get items
		$query = 'SELECT id, params'
				.' FROM #__zoo_relateditemsproxref'
				.' WHERE item_id = '.(int) $this->_item->id
				.' AND ritem_id ='.(int) $ritem
				.' AND element_id='.$this->db->Quote($this->identifier);
		
		$existing = ($res = $this->app->database->queryAssoc($query));
		
		$params = $this->_item->params->get('relateditemspro.', array());
		$params = json_encode($params);
		
		// If params are not the same
		if(array_key_exists('params', $existing) && @$existing['params'] != $params){
			// remove relation so we can reinsert it with the right params
			$query = "DELETE FROM #__zoo_relateditemsproxref"
					.' WHERE item_id = '.(int) $this->_item->id
					.' AND ritem_id ='.(int) $ritem
					.' AND element_id='.$this->db->Quote($this->identifier);
			$this->db->query($query);
		}
		
		// If the relation wasn't existing or the params weren't the same		
		if( !$existing || @$existing['params'] != $params) {			
			// add relation if entry does not exist
			$query = "INSERT INTO #__zoo_relateditemsproxref"
				." SET item_id = ".(int) $this->_item->id
				." ,ritem_id = ".(int) $ritem
				.' ,element_id='.$this->db->Quote($this->identifier)
				.' ,params='.$this->db->Quote($params); // add params too
			$this->db->query($query);
		}
		
		return true;
	}

	/*
		Function: getConfigForm
			Get parameter form object to render input form.

		Returns:
			Parameter Object
	*/
	public function getConfigForm() {
		return parent::getConfigForm()->addElementPath(dirname(__FILE__));
	}
	
	/*
		Function: bindData
			Set data through data array.

		Parameters:
			$data - array

		Returns:
			Void
	*/
	public function bindData($data = array())
	{
		// set data as JSON if item is new
		if (isset($this->_item) && $this->_item->id == 0) {
			/* when Item is new there is no Item ID and we can't store the relations in our DB, 
			   instead let's store them as JSON (ZOO way) and in next save will be moved to DB */
			parent::bindData($data);
		
		} // set data in DB if item is NOT new
		else if (isset($this->_item) && $this->_item->id != 0)
		{
			// delete item relations
			/* the related items order is the order of the rows in the DB, to let the user reorder them
			   from the Item those are deleted and created again each time */
			$query = "DELETE FROM #__zoo_relateditemsproxref"
					." WHERE item_id=".(int) $this->_item->id
					.' AND element_id='.$this->db->Quote($this->identifier);
			$this->db->query($query);
			
			if (empty($data)) return true; // if no relations return

			// save relations and it's params
			foreach($data['item'] as $key => $ritem)
			{
				// get params in the same order
				$params = array();
				foreach($data as $k => $v) if(stripos($k, 'param_') === 0){
					$new_k = str_ireplace('param_', '', $k);
					$params[$new_k] = $data[$k][$key];
				}

				// encode params
				$params = json_encode($params);

				// then save
				$query = "INSERT INTO #__zoo_relateditemsproxref"
					." SET item_id = ".(int) $this->_item->id
					." ,ritem_id = ".(int) $ritem
					.' ,element_id = '.$this->db->Quote($this->identifier)
					.' ,params = '.$this->db->Quote($params);
				
				$this->db->query($query);
			}

			// remove any JSON record
			parent::bindData(array());
		}
	}
	
}