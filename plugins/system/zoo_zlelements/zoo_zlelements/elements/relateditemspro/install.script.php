<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

	$db = JFactory::getDBO();

	if($type == 'install' || $type == 'update')
	{
		// remove old table
		$db->setQuery('DROP TABLE IF EXISTS `#__zoo_relateditemspro`')->query(); 
		
		// create new table
		$db->setQuery('CREATE TABLE IF NOT EXISTS `#__zoo_relateditemsproxref` ('
				.'`id` int(11) NOT NULL auto_increment,'
				.'`item_id` int(11) NOT NULL,'
				.'`ritem_id` int(11) NOT NULL,'
				.'`element_id` varchar(255) default NULL,'
				.'`remove` tinyint(1) default NULL,'
				.'`params` longtext NOT NULL,'
				.'PRIMARY KEY (`id`)'
				.') ENGINE=MyISAM;')->query();

		// add Param column if not exist
		$db->setQuery('SHOW COLUMNS FROM `#__zoo_relateditemsproxref`')->query();
		$fields = $db->loadObjectList();

		$field_found = false;
		foreach ($fields as $field) {
			if ($field->Field == 'params') $field_found = true;
		}

		if (!$field_found) {
			$db->setQuery('ALTER TABLE `#__zoo_relateditemsproxref` ADD COLUMN `params` longtext NOT NULL')->query();
		}

		if($type == 'install') {
			echo '<p><strong>Related Items Pro</strong> Element installed succesfully.</p>';
		} else {
			echo '<p><strong>Related Items Pro</strong> Element updated succesfully.</p>';
		}
	}
	else if($type == 'uninstall')
	{
		// message for RI Pro table drop
        $db->setQuery("SHOW TABLES LIKE '".$db->getPrefix()."zoo_relateditemsproxref%' ");
		if($msg = $db->loadResultArray()) echo "<p><strong>Related Items Pro</strong><br />To completly remove Related Items Pro you need to remove it's DB table. To do so execute this query via phpMyAdmin 'DROP TABLE ".implode(' , ',$db->loadResultArray())."'. If you don't execute the query, you will be able to install Related Items Pro again without loosing data.</p>";
	}