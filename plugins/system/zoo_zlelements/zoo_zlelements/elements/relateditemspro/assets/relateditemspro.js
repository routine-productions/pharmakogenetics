/* ===================================================
 * Related Items Pro edit script
 * https://zoolanders.com/extensions/related-items-pro
 * ===================================================
 * Copyright (C) JOOlanders SL 
 * http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 * ========================================================== */
;(function ($, window, document, undefined) {
	"use strict";
	var Plugin = function(options){
		this.options = $.extend({}, this.options, options);
	};
	$.extend(Plugin.prototype, $.zlux.Main.prototype, {
		name: 'ElementRelatedItemsPro',
		options: {
			msgEditItem: "Edit Item",
			msgDeleteItem: "Delete Item",
			msgSortItem: "Sort Item",
			msgAddItem: "Add Item",
			controlName: null,
			editable: 1,
			title: '',
			apps: '',
			types: '',
			categories: '',
			tags: '',
			authors: '',
			chosenElms: '',
			ajax_url: '',
			is_submission: 0
		},
		initialize: function(element, options) {
			this.options = $.extend({}, $.zlux.Main.prototype.options, this.options, options);
			var $this = this;


			$.zlux.assets.load('items').done(function(){
				
				// save the element reference
				$this.element = element;

				// save relations list
				$this.list = $('ul', element)

				// init sortable feature
				.sortable({
					handle: '.zl-btn-small.sort',
					placeholder: 'dragging',
					axis: 'y',
					opacity: 1,
					delay: 100,
					tolerance: 'pointer',
					containment: 'parent',
					forcePlaceholderSize: !0,
					scroll: !1,
					start: function (event, ui) {
						ui.helper.addClass('ghost')
					},
					stop: function (event, ui) {
						ui.item.removeClass('ghost')
					}
				})

				// Item Edit event
				.on('click', '.zl-btn-small.edit', function () {
					window.open($(this).data('url'), '_newtab');
				})

				// return now if not editable
				if (!$this.options.editable) return;

				// Item Relation Delete event
				$this.list.on('click', '.zl-btn-small.delete', function () {
					$(this).closest('li').fadeOut(200, function () {
						var $object = {};
						$object.id = $(this).data('id');
						$object.dom = $(this);

						$this.removeRelation($object);
					})
				})

				// save related items IDs
				$this.aRelated = [];
				$('li', $this.list).each(function(i, li){
					$this.aRelated.push(li.getAttribute('data-id'));
				})

				// set the styling class
				element.addClass('zl-bootstrap');

				// set the trigger button
				$this.dialogTrigger = $('<button type="button" title="' + $this.options.title + '" class="btn"><i class="icon-plus-sign"></i> ' + $this.options.msgAddItem + '</button>')

				// add it to dom
				.appendTo(element);

				// init the dialog manager
				$this.dialogTrigger.zlux('itemsDialogManager', {
					title: $this.options.title,
					position: {
						of: element,
						my: 'right top',
						at: 'right top'
					},
					apps: $this.options.apps,
					types: $this.options.types,
					categories: $this.options.categories,
					tags: $this.options.tags,
					authors: $this.options.authors
				})

				// on object select event
				.on("zlux.ObjectSelected", function(e, manager, $object){

					// check if already related
					if($.inArray($object.id, $this.aRelated) != '-1') {

						// if so, unrelate
						$this.removeRelation($object);

					} else { // relate

						// set the object checkbox
						$('.column-icon i', $object.dom).removeClass('icon-file-alt').addClass('icon-check');

						// append the object to the relations list dom
						$this.appendRelation($object);
					}
				})

				.on("zlux.TableDrawCallback", function(e, manager){
					
					$('tbody tr', manager.oTable).each(function(i, object_dom){
						if($.inArray(object_dom.getAttribute('data-id'), $this.aRelated) != '-1') {
							$('.column-icon i', object_dom).removeClass('icon-file-alt').addClass('icon-check');
						}
					})

					// save the manager
					$this.manager = manager;
				})


				// return now if bi-relation not set
				if (!$this.options.editable) return;

				/* Bi-relation */
				$this.list.on('click', '.zl-btn-small.delete', function () {
					var id = $(this).closest('li').data('id');
					$.ajax({
						url: $this.options.ajax_url,
						data: {
							method: "addtoremovalqueue",
							"args[0]": id,
							"args[1]": $this.options.chosenElms
						},
						type: "post",
						datatype: "json"
					})
				})
			});
		},
		appendRelation: function ($object) {
			var $this = this;
			
			// prepare object dom
			$('<li data-id="' + $object.id + '"><div>'
				+'<div class="item-name">' + $object.name + '</div>'
				+'<span class="item-tools">'
					+ ($this.options.is_submission ? '' : '<span class="zl-btn-small edit action" title="' + $this.options.msgEditItem + '" data-url="index.php?option=com_zoo&controller=item&task=edit&changeapp=' + $object.application.id + '&cid[]=' + $object.id + '"></span>')
					+'<span class="zl-btn-small delete action" title="' + $this.options.msgDeleteItem + '"></span>'
					+'<span class="zl-btn-small sort action" title="' + $this.options.msgSortItem + '"></span>'
				+'</span>'
				+'<div class="item-info">'
					+'<div class="item-type"><span class="zl-btn-small type"></span>' + $object.type.name + '</div>'
					+'<div class="item-app"><span class="zl-btn-small app"></span>' + $object.application.name + '</div>'
				+'</div>'
				+'<input type="hidden" name="' + $this.options.controlName + '" value="' + $object.id + '"/>'
			+'</div></li>')

			// add to dom
			.appendTo($this.list)

			// add to related list
			$this.aRelated.push($object.id);
		},
		removeRelation: function ($object) {
			var $this = this;
			
			// remove from dom
			$('li[data-id="' + $object.id + '"]', $this.list).remove();

			// and from related array
			$this.aRelated.splice($.inArray($object.id, $this.aRelated), 1);

			// remove the checkbox from the manager
			if ($this.manager) {
				$('tbody tr[data-id="' + $object.id + '"] .column-icon i', $this.manager.oTable).removeClass('icon-check').addClass('icon-file-alt');
			}
		}
	});
	// Don't touch
	$.fn[Plugin.prototype.name] = function() {
		var args   = arguments;
		var method = args[0] ? args[0] : null;
		return this.each(function() {
			var element = $(this);
			if (Plugin.prototype[method] && element.data(Plugin.prototype.name) && method != 'initialize') {
				element.data(Plugin.prototype.name)[method].apply(element.data(Plugin.prototype.name), Array.prototype.slice.call(args, 1));
			} else if (!method || $.isPlainObject(method)) {
				var plugin = new Plugin();
				if (Plugin.prototype['initialize']) {
					plugin.initialize.apply(plugin, $.merge([element], args));
				}
				element.data(Plugin.prototype.name, plugin);
			} else {
				$.error('Method ' +  method + ' does not exist on jQuery.' + Plugin.name);
			}
		});
	};
})(jQuery, window, document);