<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// init vars
$widgetkit = JFile::exists(JPATH_ADMINISTRATOR.'/components/com_widgetkit/widgetkit.php');
$link_enabled = !empty($img['link']);
$lightbox = !$params->find('specific._link_to_item') && $img['lightbox_image'] && $link_enabled && $widgetkit;

// set link attributes
$link_attr['target'] = $img['target'] ? 'target="_blank"' : '';
if ($widgetkit) {
	$link_attr['rel']	= $img['rel'] ? 'data-lightbox="'.$img['rel'].'"' : '';
} else {
	$link_attr['rel']	= $img['rel'] ? 'rel="'.$img['rel'].'"' : '';
}
$link_attr['title']	= $img['title'] ? 'title="'.$img['title'].'"' : '';

if ($widgetkit && !$params->find('specific._link_to_item', false) && !$img['link'] && $img['lightbox_image']) {
    $img['link'] = JURI::root() . $this->app->path->relative($this->app->zoo->resizeImage(JPATH_ROOT.'/'.$img['lightbox_image'], 0 , 0));
}

// set img attributes
$img_attr = array();
$img_attr[] = 'src="'.$img['fileurl'].'"';
$img_attr[] = 'alt="'.$img['alt'].'"';
$img_attr[] = 'width="'.$img['width'].'"';
$img_attr[] = 'height="'.$img['height'].'"';
$img_attr[] = $link_attr['title'];

// overlay / spotlight
$overlay = $spotlight = '';
if (strlen($img['spotlight_effect']) && $widgetkit) {
	if ($img['spotlight_effect'] != 'default') {
		$caption = strlen($img['caption']) ? strip_tags($img['caption']) : $img['filename'];
		$spotlight = 'data-spotlight="effect:'.$img['spotlight_effect'].';"';
		$overlay = '<div class="overlay">'.$caption.'</div>';
	} else {
		$spotlight = 'data-spotlight="on"';
	}
}

// remove empty values
$link_attr = array_filter($link_attr);
$img_attr = array_filter($img_attr); 

// set img
$content = '<img '.implode(' ', $img_attr).' />'.$overlay;

?>

<?php if ($link_enabled || $lightbox) : ?>
	<a href="<?php echo JRoute::_($img['link']); ?>" <?php echo implode(' ', $link_attr); ?><?php echo $lightbox ? ' data-lightbox="on"' : ''; ?> <?php echo $spotlight; ?>><?php echo $content; ?></a>
<?php elseif ($spotlight) : ?>
	<div <?php echo $spotlight; ?>><?php echo $content; ?></div>
<?php else : ?>
	<?php echo $content; ?>
<?php endif;