Changelog
------------

3.1.1
 + Resize on upload
 + Spotlight / Lightbox integration on default layout
 # Bugs fixed

3.1
 ^ ZLFW 3.1 compatibility
 # Fixed minor issues

3.0.3
 # Fixed Plupload issue if Max File Size was undefined
 # Submission reordering fixed

3.0.2
 + Max Image Size in Submission Advanced Mode
 # No cache layout fixed
 # Submission related issues fixed
 ^ ZLFW 3.0.10 compatibility

3.0.1
 + Individual show/hide Custom Options in Item Edit View

3.0
 ^ ZOO & Joomla 3 compatibility
 - Removed j1.5 compatibility
 + Instance Limit Reached text translation
 + ShortCuts implementation on Default layout

2.6
 # Sublayout _raw path params recovered
 # Lightbox/Spotlight Submission values fixed

2.5.7
 ^ ZLFW 2.5.8 Compatibility
 # Submission Link values on Advanced Layout fixed
 # General WK Integration issues fixed
 + WK Gallery Slideset

2.5.6
 ^ ZLFW 2.5.6 Compatibility

2.5.5
 # Fixed lightbox issue introduced in 2.5.4

2.5.4
 # Fixed several bugs with false file values

2.5.3
 ^ Language Strings cleanup
 ^ Submissions updated
 ^ Backend UI perfomance improved
 ^ ZLFW 2.5.3 Compatibility

2.5.2
 ^ ZLFW 2.5.1 Compatibility

2.5.1 RC1 - Stable
 # Fixed Lightbox issue presented in RC7
 ^ Adapted to ZLFW 2.5 RC13
 + WK Spotlight/Lightbox options per each Instance
 # Custom MimeTypes on Submission
 # Image URL for sub Joomla installations
 # Fixed sublayout rendering
 ^ Input is editable now
 + WK Integration
 + Submission support
 + File Managment
 ^ ZLFW integration

2.5
 Initial Release for ZOO 2.5


* -> Security Fix
# -> Bug Fix
$ -> Language fix or change
+ -> Addition
^ -> Change
- -> Removed
! -> Note