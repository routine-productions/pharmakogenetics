/* Copyright (C) YOOtheme GmbH, YOOtheme Proprietary Use License (http://www.yootheme.com/license) */

function selectZooItem(d, c, a) {
    jQuery("#" + a + "_id").val(d);
    jQuery("#" + a + "_name").val(c);
    SqueezeBox.close ? SqueezeBox.close() : $("sbox-window").close()
}(function ($) {
    var c = function () {};
    $.extend(c.prototype, {
        name: "ZooItem",
        options: {
            url: null,
            msgSelectItem: "Select Item"
        },
        initialize: function (a, b) {
            this.options = $.extend({}, this.options, b);
            var e = this;
            this.app = a.find(".application select");
            this.item = a.find("div.item");
            
            this.app.bind("change", function () {
                e.setValues();
                e.item.length && (e.item.find("input:hidden").val(""), e.item.find("input:text").val(e.options.msgSelectItem))
            });
			e.setValues();
        },
        setValues: function () {
            var a = this.app.val();
            this.item.length && (a && this.item.find("a").attr("href", this.options.url + "&app_id=" + a))
        }
    });
    $.fn[c.prototype.name] = function () {
        var a = arguments,
            b = a[0] ? a[0] : null;
        return this.each(function () {
            var e = $(this);
            if (c.prototype[b] && e.data(c.prototype.name) && b != "initialize") e.data(c.prototype.name)[b].apply(e.data(c.prototype.name), Array.prototype.slice.call(a, 1));
            else if (!b || $.isPlainObject(b)) {
                var f = new c;
                c.prototype.initialize && f.initialize.apply(f, $.merge([e], a));
                e.data(c.prototype.name, f)
            } else $.error("Method " + b + " does not exist on jQuery." + c.name)
        })
    }
})(jQuery);