<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

class JElementModuleList extends JElement {

	var	$_name = 'ModuleList';

	function fetchElement($name, $value, &$node, $control_name) {
	
		// init var
		$app = App::getInstance('zoo');
	
		$options = array($app->html->_('select.option', '', '- '.JText::_('Select Module').' -'));
		return $app->html->_('zoo.modulelist', $options, $control_name.'[module]', null, 'value', 'text', $value);

	}
}