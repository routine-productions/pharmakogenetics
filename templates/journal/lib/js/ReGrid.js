
var Grid = {};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Multi Rows
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Grid.MultiRow = function ()
{
    // Default Variables
    var Grid_Class = '#yoo-zoo .Grid',         // Used Grid Class Names
        Node_Class = 'Grid-Node',                    // Node Class Name
        Class_Open_Row = 'Row-Open',                // Default Row-Open Class Name
        Class_Close_Row = 'Row-Close',               // Default Row-Close Class Name
        Class_First_Row = 'Row-First';               // Default Row-First Class Name

    // Each Grid
    jQuery(Grid_Class).each(function (GridKey, Grid)
    {
        var Grid_Width = jQuery(Grid).width();

        var Row_Width = 0;
        var Row_Count = 1;

        // Each Node
        jQuery(Grid).find('>[class*=' + Node_Class + ']').each(function (Node_Key, Node)
        {
            var Node_Width = Math.floor(jQuery(Node).outerWidth(true));

            var Node_Margin = parseInt(jQuery(Node).css('margin-right'));

            // First Node
            if (Node_Key == 0)
            {
                jQuery(Node).addClass(Class_Open_Row).addClass('Row-' + Row_Count);
                Row_Width += Math.floor(jQuery(Node).outerWidth(true));
            }

            else
            {

                // Row Growth
                if ((Row_Width + Node_Width - Node_Margin) <= Grid_Width)
                {
                    Row_Width += Node_Width;
                }

                // Row Full
                else
                {
                    jQuery(Node).prev().addClass(Class_Close_Row);
                    jQuery(Node).addClass(Class_Open_Row);

                    Row_Width = Math.floor(jQuery(Node).outerWidth(true));
                    // Count Nodes

                    Row_Count++;

                    // Last Node
                    if (!jQuery(Node).next().length)
                    {
                        jQuery(Node).addClass(Class_Close_Row);
                    }
                }

            }
            jQuery(Node).addClass('Row-' + Row_Count);

        });
    });

    Grid.MultiRow.Reset = function ()
    {
        jQuery(Grid_Class + ' ' + Node_Class).removeClass(Class_Open_Row + ' ' + Class_Close_Row + ' ' + Class_First_Row);
		jQuery(Grid_Class + ' ' + Node_Class).removeAttr('style');
		jQuery(Grid_Class).removeAttr('style');
    }
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////






////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Vertical
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Grid.Vertical = function ()
{
    var Grid_Class = '#yoo-zoo .Grid',         // Used Grid Class Names
        Node_Class = 'Grid-Node';

    // Each Grid
    jQuery(Grid_Class).each(function (GridKey, Grid)
    {
        var Nodes_Difference = [];
        var Nodes_Full_Height = [];
        var Grid_Height = 0;
        // Each Row
        for (var RowCounter = 1; ; RowCounter++)
        {
            if (jQuery('.Row-' + RowCounter, Grid).length > 0)
            {

                // Current
                var Nodes_Height = jQuery('.Row-' + RowCounter, Grid).map(function ()
                {
                    return jQuery(this).height();
                });

                var Nodes_Outer_Height = jQuery('.Row-' + RowCounter, Grid).map(function ()
                {
                    return jQuery(this).outerHeight(true);
                });

                // Add
                jQuery.each(Nodes_Outer_Height, function (ItemKey, Item)
                {
                    if (Nodes_Full_Height[ItemKey] != undefined)
                    {
                        Nodes_Full_Height[ItemKey] += Item;
                    } else
                    {
                        Nodes_Full_Height[ItemKey] = Item;
                    }
                });

				
                var Nodes_Max_Height = Math.max.apply(null, Nodes_Height);

                Grid_Height += Nodes_Max_Height;


                // Difference
                jQuery.each(Nodes_Height, function (ItemKey, Item)
                {
                    if (Nodes_Difference[ItemKey] != undefined)
                    {
                        Nodes_Difference[ItemKey] = Nodes_Difference[ItemKey] + Nodes_Max_Height - Item;
                    } else
                    {
                        Nodes_Difference[ItemKey] = Nodes_Max_Height - Item;
                    }
                });

                // Deduction
                jQuery('.Row-' + (RowCounter + 1), Grid).each(function (Node_Key, Node)
                {
                    jQuery(Node).css('top', '-' + Nodes_Difference[Node_Key] + 'px');
                });
            }
            else
            {
                break;
            }
        }
        Grid_Height = Math.max.apply(null, Nodes_Full_Height);
        console.log(Nodes_Full_Height);
        jQuery(Grid).css('height', Grid_Height + 'px');
    });
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////






////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Start
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*jQuery(document).ready(function(){
    jQuery('img').load(function()
    {

        Grid.MultiRow();
        Grid.Vertical();



        jQuery(window).resize(function ()
        {
            Grid.MultiRow.Reset();
            Grid.MultiRow();
            Grid.Vertical();
        });


    });
});*/


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
